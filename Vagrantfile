# Mohd Zulfadzly Abdol Razak
# mzulfadzly@yahoo.com
# -*- mode: ruby -*-
# vi: set ft=ruby :

require 'yaml'

module OS
    def OS.windows?
        (/cygwin|mswin|mingw|bccwin|wince|emx/ =~ RUBY_PLATFORM) != nil
    end

    def OS.mac?
        (/darwin/ =~ RUBY_PLATFORM) != nil
    end

    def OS.unix?
        !OS.windows?
    end

    def OS.linux?
        OS.unix? and not OS.mac?
    end
end

if OS.windows?
    host_os = "windows"
elsif OS.mac?
    host_os = "mac"
elsif OS.unix?
    host_os = "unix"
elsif OS.linux?
    host_os = "linux"
else
    host_os = "unknown platform."
end


current_dir=File.dirname(File.expand_path(__FILE__))
params = YAML::load_file("#{current_dir}/config.yaml")


Vagrant.configure(2) do |config|
  config.vm.box = "bento/ubuntu-16.04"
  config.vm.network "forwarded_port", guest: params['box_client_http_port'],    host: params['box_host_http_port'], auto_correct: true
  config.vm.network "forwarded_port", guest: params['box_client_mysql_port'],  host: params['box_host_mysql_port'], auto_correct: true
  config.vm.network "forwarded_port", guest: params['box_client_mongo_port'], host: params['box_host_mongo_port'], auto_correct: true
  config.vm.synced_folder params['project_root_dir_host'], params['project_root_dir_guest'] ,
  create:true ,group:"www-data" ,owner:"vagrant" ,:mount_options => ["dmode=777", "fmode=777"]
  config.ssh.shell = "bash -c 'BASH_ENV=/etc/profile exec bash'"
  config.vm.provision :file do |file|
    file.source = "~/.gitconfig"
    file.destination = "~/.gitconfig"
  end
  config.vm.provision :file do |file|
    file.source = "~/.ssh/id_rsa"
    file.destination = "~/.ssh/id_rsa"
  end
  config.vm.provision :file do |file|
    file.source = "~/.ssh/id_rsa.pub"
    file.destination = "~/.ssh/id_rsa.pub"
  end
  config.ssh.forward_agent = true
  config.ssh.insert_key = false
  config.vm.network "private_network", ip: params['box_ip']
  config.vm.provider "virtualbox" do |vb|
   vb.memory = params['box_ram']
   vb.cpus = params['box_cpu']
  end
end
