# README #

Vagrantfile to spin up Ubuntu Bento 16.04


## Steps ##

1. Create working directory


```
#!python

exm : mkdir ubuntuBox
```

2. Navigate to the working directory


```
#!python

exm : cd ubuntuBox
```

3. Clone this repo


```
#!python

git clone git@bitbucket.org:zulfadzly/ubuntu-16.04-base-vagrant.git .
```

4.Start Vagrant


```
#!python

vagrant up

```